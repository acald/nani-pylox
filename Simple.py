import sys

import Scanner

number_of_args = len(sys.argv)
args = sys.argv


def report(line, where, message):
    print(f'[line {line}] Error {where}: {message}')


def error(line, where='', message=None):
    report(line, where, message)


class Simple:
    def __init__(self):
        self.had_error = False

    def run_file(self, path):
        file = open(path, 'r')
        input_code = file.read()
        file.close()

        self.run(input_code)

    def run_prompt(self, input_code=None):
        while True:
            line = input('> ')
            if line == '<exit REPL>':
                break

            self.run(input_code)

    def run(self, input_code):
        local_scanner = Scanner(input_code, self)
        tokens = list(local_scanner.scan_tokens())

        for token in tokens:
            print(token)

        if self.had_error:
            sys.exit(65)


simple_interpret = Simple()
if __name__ == '__main__':
    if number_of_args > 2:
        sys.exit(64)
    elif number_of_args == 2:
        simple_interpret.run_file(args[1])
    else:
        simple_interpret.run_prompt()
