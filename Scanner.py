import Simple
from Token import Token
from TokenType import TokenType


class Scanner:
    def __init__(self, source, interpreter):
        self.source = source
        self.interpreter = interpreter
        self.tokens = []
        self.start = 0
        self.current = 0
        self.line = 1
        self.digits = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9']
        self.token_strings = {
            '(': lambda c: TokenType.LEFT_PARENTHESIS,
            ')': lambda c: TokenType.RIGHT_PARENTHESIS,
            '{': lambda c: TokenType.LEFT_BRACE,
            '}': lambda c: TokenType.RIGHT_BRACE,
            ',': lambda c: TokenType.COMMA,
            '.': lambda c: TokenType.DOT,
            '-': lambda c: TokenType.MINUS,
            '+': lambda c: TokenType.ADD,
            '*': lambda c: TokenType.MULTIPLY,
            ';': lambda c: TokenType.SEMICOLON,
            '/': lambda c: TokenType.DIVIDE,
            ' ': lambda c: None,
            '\r': lambda c: None,
            '\t': lambda c: None,
            '\n': lambda c: self._advance_line(),
            '"': lambda c: self._string_logic(),
        }
        self.keywords = {
            'false': TokenType.FALSE,
            'none': TokenType.NONE,
            'true': TokenType.TRUE,
            'and': TokenType.AND,
            'as': TokenType.AS,
            'assert': TokenType.ASSERT,
            'break': TokenType.BREAK,
            'class': TokenType.CLASS,
            'continue': TokenType.CONTINUE
        }

    """
    """

    def scan_tokens(self):
        while not self.is_at_end():
            start = self.current
            self._scan_token()

        self.tokens.append(Token(TokenType.EOF, '', None, self.line))
        return self.tokens

    def _scan_token(self):
        c = self._advance()
        if c in self.token_strings:
            if self._is_digit(c):
                self._number_logic()
            else:
                c = self.token_strings[c](c)
                if c is not None:
                    self._add_token(c)
        elif self._is_digit(c):
            self._number_logic()
        else:
            Simple.error(self.line, 'Unexpected character.')

    """
    """

    def is_at_end(self):
        return self.current >= len(self.source)

    def _peek(self):
        if self.is_at_end():
            return '\0'
        return self.source[self.current]

    def _advance(self):
        self.current += 1
        return self.source[self.current - 1]

    def _string_logic(self):
        while (self._peek() != '"') and not self.is_at_end():
            if self._peek() == '\n':
                self.line += 1
            self._advance()

        if self.is_at_end():
            Simple.error(self.line, 'Unterminated string.')
            return None

        self._advance()

        value = self.source[self.start+1:self.current-1]
        self._add_token(TokenType.STRING, value)

    def _add_token(self, token_type, literal=None):
        text = self.source[self.start:self.current]
        self.tokens.append(Token(token_type, text, literal, self.line))

    def _advance_line(self):
        self.line += 1

    def _peek_next(self):
        if (self.current + 1) >= len(self.source):
            return '\0'
        return self.source[self.current + 1]

    def _is_digit(self, c):
        return c in self.digits

    def _number_logic(self):
        while self._is_digit(self._peek()):
            self._advance()

        if self._peek() == '.' and self._is_digit(self._peek_next()):
            self._advance()
            while self._is_digit(self._peek()):
                self._advance()

        self._add_token(TokenType.NUMBER, float(self.source[self.start:self.current]))

    def _match(self, expected):
        if self.is_at_end():
            return False
        if self.source[self.source] != expected:
            return False

        self.current += 1
        return True
